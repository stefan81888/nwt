import { Product } from './product';

export class Order{
    firstName: string;
    lastName: string;
    address: string;
    phoneNumber: string;
    orderedProducts: Product[];
    totalPrice: number;
}