import {MatTabsModule, MatGridListModule, MatCardModule, MatButtonModule, MatListModule, MatInputModule} from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
   imports: [MatTabsModule, MatGridListModule, MatCardModule, MatButtonModule, MatListModule, MatInputModule],
   exports: [MatTabsModule, MatGridListModule, MatCardModule, MatButtonModule, MatListModule, MatInputModule] 
})
export class MaterialModule { }