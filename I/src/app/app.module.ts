import { BrowserModule } from '@angular/platform-browser';
import {MaterialModule} from './material.module'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from '../app/components/app-component/app.component';
import { HomeComponent } from './components/app-component/home/home.component';
import { HeaderComponent } from './components/app-component/header/header.component';
import { ProductsComponent } from './components/app-component/home/products/products.component';
import { ProductComponent } from './components/app-component/home/products/product/product.component';
import { BasketComponent } from './components/app-component/home/basket/basket.component';
import { OrderComponent } from './components/app-component/home/order/order.component';
import { AddProductComponent } from './components/app-component/home/add-product/add-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ProductsComponent,
    ProductComponent,
    BasketComponent,
    OrderComponent,
    AddProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
