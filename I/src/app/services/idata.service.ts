import { Product, Order } from '../model/index';
import { Observable } from 'rxjs';

export interface IDataService{
    getAllProducts(): Observable<Product[]>;
    getProductById(id: number): Observable<Product>;
    addToBasket(product: Product): void;
    getBasketProductsCount(): number;
    getBasket(): Product[];
    makeOrder(order: Order): void;
    addProduct(product: Product): Observable<Product>;
    removeFromBasket(product: Product): void;
    getTotalPrice() : number;
}