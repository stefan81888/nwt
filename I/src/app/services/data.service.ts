import { IDataService } from './idata.service';
import { Product, Order } from '../model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: "root"
})
export class DataService implements IDataService{

    private basket: Product[] = [];
    private baseUrl: string;
    private totalPrice: number;
    
    constructor(private http: HttpClient){
        this.baseUrl = "http://localhost:3000";
        this.totalPrice = 0;
    }

    getAllProducts(): Observable<Product[]>{
        let url: string = this.baseUrl + "/products";
        return this.http.get<Product[]>(url);
    }

    getProductById(id: number): Observable<Product>{
        let url: string = this.baseUrl + "/products/" + id;
        return this.http.get<Product>(url);
    }

    addToBasket(product: Product): void{
        this.basket.push(product);
        this.totalPrice += parseInt(product.price.toString());
    }

    getBasketProductsCount(): number{
        return this.basket.length;
    }

    getBasket(): Product[]{
        return this.basket;
    }

    makeOrder(order: Order): void{
        order.orderedProducts = this.basket;
        this.basket = [];
        this.totalPrice = 0;
        let url: string = this.baseUrl + "/orders";
        this.http.post<Order>(url, order).subscribe(
            () => {}
        );
    }

    addProduct(product: Product): Observable<Product>{
        let url: string = this.baseUrl + "/products";
        return this.http.post<Product>(url, product);
    }

    removeFromBasket(product: Product): void{
        let productInArray = this.basket.find( x => x.id == product.id);
        let index = this.basket.indexOf(productInArray, 0);
        this.basket.splice(index, 1);
        this.totalPrice -= product.price;
    }

    getTotalPrice() : number{
        return this.totalPrice;
    }

}
