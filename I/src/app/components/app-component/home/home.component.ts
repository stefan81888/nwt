import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { BasketComponent } from './basket/basket.component';
import { ProductsComponent } from './products/products.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  basketStatus: string;
  basketProductsCount: number;

  @ViewChild('basketItems')
  basket: BasketComponent;  

  @ViewChild('products')
  products: ProductsComponent;  

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.basketProductsCount = this.dataService.getBasketProductsCount();
    this.basketStatus = "Basket(" + this.basketProductsCount + ")";
  }

  changeBasketStatus(productsAdded: number): void{
    this.basketProductsCount += productsAdded;
    this.basketStatus = "Basket(" + this.basketProductsCount + ")";
    this.basket.ngOnInit();
  }

  resetStatusOnOrder(count: number): void{
    this.basketProductsCount = count;
    this.basketStatus = "Basket(" + this.basketProductsCount + ")";
    this.basket.ngOnInit();
  }

  reloadProducts(): void {
    this.products.ngOnInit();
  }

}
