import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/model/index';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  products: Product[];
  @Output() notifyRemoveFromBasket: EventEmitter<number> = new EventEmitter<number>();

  totalPrice: number;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.products = this.dataService.getBasket();
    this.totalPrice = this.dataService.getTotalPrice();
  }

  remove(products: Product): void{
    this.dataService.removeFromBasket(products);
    this.notifyRemoveFromBasket.emit(-1);
    this.ngOnInit();
  }

}
