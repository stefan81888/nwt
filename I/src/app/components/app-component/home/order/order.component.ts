import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Order } from 'src/app/model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  @ViewChild('inputForm')
  form: NgForm;  
  firstName: string;
  lastName: string;
  phone: string;
  address: string;

  @Output() notifyOrder: EventEmitter<number> = new EventEmitter<number>();


  constructor(private dataService: DataService) { }

  order(): void {
    let order: Order = {
      phoneNumber: this.phone,
      address: this.address,
      firstName: this.firstName,
      orderedProducts: [],
      lastName: this.lastName,
      totalPrice: this.dataService.getTotalPrice()
    }
    this.dataService.makeOrder(order);
    this.form.reset();
    this.notifyOrder.emit(0);
  }

  ngOnInit() {
  }

}
