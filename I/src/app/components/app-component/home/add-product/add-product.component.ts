import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/model';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  @ViewChild('inputForm')
  form: NgForm;  

  @Output()
  notifyAddProduct: EventEmitter<any> = new EventEmitter<any>();

  name: string;
  description: string;
  imageUrl: string;
  price: number;

  constructor(private dataService: DataService) { }

  add(): void{
    let product: Product = {
      name: this.name,
      description: this.description,
      imageUrl: this.imageUrl,
      id: "",
      price: this.price
    }
    this.dataService.addProduct(product).subscribe(
      () => {
        this.notifyAddProduct.emit();
      }
    );
    this.form.reset();
  }

  ngOnInit() {
  }

}
