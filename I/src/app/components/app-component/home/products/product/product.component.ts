import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Product } from 'src/app/model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product: Product;

  constructor(private dataService: DataService, private route: ActivatedRoute) {
    let productId: number;
    this.route.params.subscribe((params)=> {
      productId = params['id'];
      this.dataService.getProductById(productId).subscribe(
        (data: Product) => {
          this.product = data;
        }
      );
    });

   }

  ngOnInit() {
  }

  addToBasket(): void{
    this.dataService.addToBasket(this.product);
  }

}
