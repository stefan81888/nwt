import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/index';
import { DataService } from 'src/app/services/data.service';
import { EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  @Output() notifyAddToBasket: EventEmitter<number> = new EventEmitter<number>();
  
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getAllProducts().subscribe(
      (data: Product[]) => {
        this.products = data;
      }
    );
  }

  addToBasket(product: Product): void{
    this.dataService.addToBasket(product);
    this.notifyAddToBasket.emit(1);
  }


}
